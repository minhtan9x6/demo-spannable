package com.example.tantan.spannablestringdemo;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView tvHello;
    private SpannableString mSpannableString;
    private ClickableSpan mClickableSpan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        tvHello = (TextView) findViewById(R.id.tv_hello);
        tvHello.setMovementMethod(LinkMovementMethod.getInstance());
        tvHello.setText(generateSpannable("cyka 012456789", 5, 14));
    }

    private SpannableString generateSpannable(String s, int from, int to) {
        final String phoneNum = s.substring(from,to);
        mSpannableString = new SpannableString(s);
        mClickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + phoneNum));
                Log.i("Phone",phoneNum);
                startActivity(intent);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                ds.setColor(getResources().getColor(R.color.colorPrimaryDark));
                ds.setUnderlineText(true);
            }
        };
        mSpannableString.setSpan(mClickableSpan, from, to, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        return mSpannableString;
    }


}
